#Riddle starts from universe_of_lines.html files
#Each Correct answer redirects the page to another file


#Hints

# 1 -> the riddle means qr so inspect code to get qr image. Ans in QR code is level8
# 2 -> Ans is ^!xVR*CT=P!#mm7tf  . It can be seen after the audio bar goes off in 30 second
#3 -> Auto detects the clients IP and gives Hello message. Task is to understand that the hint is about public IP and answer should be +1 to each number
#     For now hints will be in screen.